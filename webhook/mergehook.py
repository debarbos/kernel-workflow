"""Ensure a merge request is mergeable."""
import subprocess
import sys
import typing

from cki_lib import logger
from cki_lib import misc

from . import common
from . import defs
from . import fragments
from . import kgit
from .description import MRDescription
from .session import new_session

if typing.TYPE_CHECKING:
    from .session import SessionRunner
    from .session_events import GitlabMREvent
    from .session_events import GitlabNoteEvent

LOGGER = logger.get_logger('cki.webhook.mergehook')


# Get all open MRs.
MR_QUERY_BASE = """
query mrData($namespace: ID!, $branches: [String!], $first: Boolean = true, $after: String = "") {
  project(fullPath: $namespace) {
    id @include(if: $first)
    mergeRequests(state: opened, after: $after, targetBranches: $branches) {
      pageInfo {hasNextPage endCursor}
      nodes {
        iid
        author {username}
        title
        targetBranch
        project {fullPath}
        ...MrLabels
        ...MrCommits
      }
    }
  }
}
"""

MR_QUERY = MR_QUERY_BASE + fragments.MR_LABELS + fragments.MR_COMMITS + fragments.GL_USER


def fetch_mr_list(gl_project, gl_mergerequest, graphql):
    """Fetch the list of relevant MRs via graphql query."""
    query_params = {'namespace': gl_project.path_with_namespace,
                    'branches': gl_mergerequest.target_branch}
    result = graphql.check_query_results(
        graphql.client.query(MR_QUERY, query_params, paged_key="project/mergeRequests"),
        check_keys={'project'})
    mr_list = [mr for mr in misc.get_nested_key(result, 'project/mergeRequests/nodes')
               if mr['targetBranch'] == gl_mergerequest.target_branch]

    LOGGER.debug("Returning mr list of %s", mr_list)
    return mr_list


def build_mr_conflict_string(mr_entry):
    """Build MR conflict report string."""
    mr_id = int(mr_entry['iid'])
    author = mr_entry['author']['username']
    title = mr_entry['title']

    return f"MR !{mr_id} from @{author} (`{title}`) conflicts with this MR.  \n"


def validate_matching_hashes(gl_mergerequest, dep_mr):
    """Check to see if all the hashes of the commits in dep_mr exist in gl_mergerequest."""
    mr_commits = [commit.id for commit in gl_mergerequest.commits()]
    for commit in dep_mr['commits']['nodes']:
        if commit['sha'] not in mr_commits:
            return False

    return True


def check_for_other_merge_conflicts(gl_project, gl_mergerequest,
                                    graphql, merge_branch, worktree_dir, remote_name):
    # pylint: disable=too-many-arguments
    """Check to see if there are any other pending MRs that conflict with this MR."""
    LOGGER.debug("Checking other pending %s MRs for conflicts with MR %d",
                 gl_mergerequest.target_branch, gl_mergerequest.iid)

    mr_list = fetch_mr_list(gl_project, gl_mergerequest, graphql)
    # Save the base mr-is-merged as a branch, so we can quickly reset after each OTHER mr
    save_branch = f"{merge_branch}-save"
    kgit.branch_copy(worktree_dir, save_branch)
    base_mr_desc = MRDescription(gl_mergerequest.description,
                                 gl_project.path_with_namespace, graphql)

    conflicts = []
    target_branch = f"{remote_name}/{gl_mergerequest.target_branch}"
    for mr_entry in mr_list:
        mr_id = int(mr_entry['iid'])
        if mr_id == gl_mergerequest.iid:
            continue
        # First, make sure this MR can actually be merged by itself
        if {'title': defs.MERGE_CONFLICT_LABEL} in mr_entry['labels']['nodes']:
            LOGGER.debug("MR %d has conflicts label on it, skipping it", mr_id)
            continue
        if not kgit.branch_mergeable(worktree_dir, target_branch,
                                     f'{remote_name}/merge-requests/{mr_id}'):
            LOGGER.debug("MR %d can't be merged by itself, skipping conflict check", mr_id)
            continue

        if mr_id in base_mr_desc.depends_mrs:
            LOGGER.debug("MR %d is a stated dependency of MR %d", mr_id, gl_mergerequest.iid)
            if not validate_matching_hashes(gl_mergerequest, mr_entry):
                conflicts.append("CONFLICT: your dependency MR has different hashes from the ones "
                                 "included in your MR. Please rebase this MR on top of the "
                                 f"current version of !{mr_id}.")

        # Now, try to merge it on top of this MR, so we can see if it has any conflicts
        kgit.hard_reset(worktree_dir, save_branch)
        try:
            kgit.merge(worktree_dir, f'{remote_name}/merge-requests/{mr_id}')
        except subprocess.CalledProcessError as err:
            conflicts.append(build_mr_conflict_string(mr_entry))
            conflicts.append(err.output)

    kgit.branch_delete(worktree_dir, save_branch)
    return conflicts


def check_for_merge_conflicts(remote_name, gl_mergerequest, worktree_dir):
    """Check to see if this MR can be cleanly merged to the target branch."""
    mr_id = gl_mergerequest.iid
    target_branch = f"{remote_name}/{gl_mergerequest.target_branch}"
    LOGGER.debug("Checking for merge conflicts when merging %d into %s.", mr_id, target_branch)
    conflicts = []
    kgit.create_worktree_timestamp(worktree_dir)
    # Now try to merge the MR to the worktree
    try:
        kgit.merge(worktree_dir, f'{remote_name}/merge-requests/{mr_id}')
    except subprocess.CalledProcessError as err:
        kgit.hard_reset(worktree_dir, target_branch)
        conflicts.append(f"MR !{mr_id} cannot be merged to {target_branch}  \n")
        conflicts.append(err.output)
    return conflicts


def format_conflict_info(conflict_info, merge_label):
    """Format the additional merge conflict output for comment output."""
    if merge_label == defs.MERGE_CONFLICT_LABEL:
        comment = "This merge request cannot be merged to its target branch\n\n"
    elif merge_label == defs.MERGE_WARNING_LABEL:
        comment = ('There are other pending MRs that conflict with this one. '
                   'Please discuss possible merge solutions with the author(s) of the other '
                   'merge request(s).\n\n')
    else:
        comment = "This MR can be merged cleanly to its target branch."

    for entry in conflict_info:
        if entry.startswith('MR'):
            comment += f'\n{entry}\n'
        else:
            for line in entry.split('\n'):
                if line.startswith('CONFLICT'):
                    comment += f'* {line}  \n'

    return comment


def process_gl_event(
    _: dict,
    session: 'SessionRunner',
    event: typing.Union['GitlabMREvent', 'GitlabNoteEvent'],
    **__: typing.Any
) -> None:
    """Process a gitlab event."""
    LOGGER.info('Processing %s event for %s', event.kind.name, event.mr_url)
    if not (gl_mergerequest := event.gl_mr):
        return
    gl_project = session.get_gl_project(event.namespace)
    rhkernel_src = session.args.rhkernel_src
    merge_label = f'Merge::{defs.READY_SUFFIX}'
    remote_name = gl_project.name if not event.rh_project.confidential else \
        f'{gl_project.name}-private'
    kgit.update_project_refs(remote_name, rhkernel_src)
    merge_branch, worktree_dir = kgit.prep_temp_merge_branch(remote_name, gl_mergerequest,
                                                             rhkernel_src)

    conflict_info = check_for_merge_conflicts(remote_name, gl_mergerequest, worktree_dir)
    if conflict_info:
        # if the MR can't be merged to target branch, try to force gitlab's status recheck before
        # we return our git-based conflicts status
        if conflict_info and misc.is_production_or_staging():
            gl_project.mergerequests.list(iids=[gl_mergerequest.iid],
                                          with_merge_status_recheck=True)
        merge_label = defs.MERGE_CONFLICT_LABEL
    else:
        conflict_info = check_for_other_merge_conflicts(gl_project, gl_mergerequest,
                                                        session.graphql, merge_branch, worktree_dir,
                                                        remote_name)
        if conflict_info:
            merge_label = defs.MERGE_WARNING_LABEL

    note = f'**Mergeability Summary:** ~"{merge_label}"\n\n'
    note += format_conflict_info(conflict_info, merge_label)

    LOGGER.info("Conflict info:\n%s", note)
    kgit.clean_up_temp_merge_branch(rhkernel_src, merge_branch, worktree_dir)
    common.add_label_to_merge_request(gl_project, gl_mergerequest.iid, [merge_label])

    session.update_webhook_comment(gl_mergerequest, note,
                                   bot_name=session.gl_instance.user.username,
                                   identifier='**Mergeability Summary:')


HANDLERS = {
    defs.GitlabObjectKind.MERGE_REQUEST: process_gl_event,
    defs.GitlabObjectKind.NOTE: process_gl_event,
}


def main(args):
    """Run main loop."""
    parser = common.get_arg_parser('MERGEHOOK')
    parser.add_argument('--rhkernel-src', **common.get_argparse_environ_opts('RHKERNEL_SRC'),
                        help='Directory containing RH kernel projects git tree')
    args = parser.parse_args(args)
    if not args.rhkernel_src:
        LOGGER.warning("No Linux source tree directory specified, using default")
    session = new_session('mergehook', args, HANDLERS)
    session.run()


if __name__ == '__main__':
    main(sys.argv[1:])
