---

branch: &branch
  title: branch
  description: A Project branch.
  type: object
  properties:
    components:
      description: List of product components built from this branch.
      type: array
      items: {
        enum: ["kernel", "kernel-rt", "kernel-automotive"]
      }
    distgit_ref:
      description: Dist-git branch name this source git branch corresponds to.
      type: string
    fix_versions:
      description: List of possible FixVersions values for a RHEL Jira issue targeting this branch.
      type: array
      items: {
        type: string
      }
    inactive:
      description: Whether development is actively being done in this branch.
      type: boolean
      default: true
    internal_target_release:
      description: Bugzilla Internal Target Release (ITR) this branch corresponds to.
      type: string
      default: ""
    milestone:
      description: Milestone associated with this branch.
      type: string
      default: ""
    name:
      description: Name of the branch.
      type: string
    pipelines:
      description: List of expected pipeline types for this branch.
      type: array
      items: {
        type: string
      }
    protected_approval_rules:
      description: Unused.
      type: boolean
      default: true
    sub_component:
      description: Subcomponent of BZs targeting this branch.
      type: string
      default: ""
    zstream_target_release:
      description: Bugzilla Zstream Target Release (ZTR) this branch corresponds to.
      type: string
      default: ""
  required: ["components", "distgit_ref", "name"]
  additionalProperties: false

project: &project
  title: project
  description: A Gitlab project.
  type: object
  properties:
    branches:
      description: List of protected branches in the project.
      type: array
      items: *branch
    confidential:
      description: Whether this is a confidential project or not.
      type: boolean
      default: false
    default_branch:
      description: Name of the default branch in the project.
      type: string
    id:
      description: Gitlab project global ID.
      type: integer
    group_id:
      description: Gitlab project's group global ID.
      type: integer
    group_labels:
      description: Whether label API calls should be done at the group level (versus project).
      type: boolean
      default: true
    inactive:
      description: Whether development is actively being done in this project. When False, overrides Branch property of the same name.
      type: boolean
      default: true
    pipelines:
      description: Default list of expected pipeline types for this project's branches.
      type: array
      items: {
        type: string
      }
    product:
      description: Name of the product this project is a part of.
      type: string
    name:
      description: Gitlab project name.
      type: string
    namespace:
      description: Gitlab project path_with_namespace.
      type: string
    public_signoff_ok:
      description: Whether the DCO signoff check will allow a non-@redhat.com email address.
      type: boolean
      default: false
    sandbox:
      description: Whether this is a sandbox (staging) project or not.
      type: boolean
      default: false
    webhooks:
      description: Mapping with webhook names and boolean values indicating whether they should be enabled on this project or not.
      type: object
      patternProperties: {
        "^[A-Za-z_][A-Za-z0-9_]*$": {
          type: boolean
        }
      }
      additionalProperties: false

  required: ["branches", "default_branch", "id", "group_id", "pipelines", "product", "name", "namespace"]
  additionalProperties: false


schema:
  $schema: https://json-schema.org/draft/2020-12/schema
  $id: https://gitlab.com/cki-project/kernel-workflow/-/raw/main/webhook/schema_rh_projects.yaml
  title: Schema for Gitlab 'projects' metadata
  description: Properties of the ark/centos/rhel kernel Gitlab projects.
  type: object
  properties:
    projects:
      description: List of Gitlab projects.
      type: array
      items: *project
  required: ["projects"]
  additionalProperties: true
