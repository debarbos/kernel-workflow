# component_webview

A prototype front end to display the data aggregated by:
[component_jsonify](https://gitlab.cee.redhat.com/debarbos/component_jsonify/)

install and run locally:

``` bash
python3 -m venv venv
source venv/bin/activate
pip install .
python src/component_webview/main.py
```

use the container files to run in a docker container:
`sudo docker-compose up`
