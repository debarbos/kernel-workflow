"""Basic cache module."""
import typing

from cki_lib.misc import get_logger

LOGGER = get_logger('cki.webhook.cache')


class BaseCache:
    # pylint: disable=too-few-public-methods
    """A base class for a cache."""

    _data_type = dict

    def __init__(
        self,
        *,
        data: typing.Optional[typing.Any] = None,
        populate: bool = False,
        **kwargs
    ) -> None:
        """Log yourself."""
        LOGGER.debug('Constructing a %s', self.__class__.__name__)
        data = self._populate(**kwargs) if populate else data or self._data_type()
        self.data = self._check_data(data)
        LOGGER.info('Created %s', self)

    def __repr__(self) -> str:
        """Tell us something about yourself."""
        return f'<{self.__class__.__name__}, items: {self._size()}>'

    @classmethod
    def _check_data(cls, data: typing.Any) -> typing.Any:
        """Check the input data is the expected type."""
        if data is not None and not isinstance(data, cls._data_type):
            raise ValueError(f"{cls.__name__} expected a {cls._data_type}, got {type(data)}")
        return data

    def _populate(self, **_) -> typing.Any:
        """Populate self.dict."""
        return self._data_type()

    def _size(self) -> int:
        """Return the size of the cache."""
        return len(self.data)
